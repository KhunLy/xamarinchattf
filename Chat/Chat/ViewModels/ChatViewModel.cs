﻿using Chat.Models;
using Chat.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace Chat.ViewModels
{
    public class ChatViewModel: ViewModelBase
    {
        private string _Content;

        public string Content
        {
            get { return _Content; }
            set { SetValue(ref _Content, value); }
        }


        private Command _SendCommand;

        public Command SendCommand
        {
            get {
                return _SendCommand = _SendCommand
                  ?? new Command(Send);
            }
        }

        private Command _DeleteCommand;

        public Command DeleteCommand
        {
            get
            {
                return _DeleteCommand = _DeleteCommand
                  ?? new Command<Message>(Delete);
            }
        }

        private void Delete(Message obj)
        {
            DependencyService.Get<IMessageService>()
                .Delete(obj.Key);
        }

        public ObservableCollection<Message> Messages { get; set; }

        public ChatViewModel()
        {
            Messages = new ObservableCollection<Message>();

            DependencyService.Get<IMessageService>().OnAdd
                += (message) =>
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        Messages.Add(message);
                    });
                    
                };
            DependencyService.Get<IMessageService>().OnDelete
                += (message) =>
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        var toR = Messages.FirstOrDefault(m => m.Key == message.Key);
                        Messages.Remove(toR);
                    });
                    // remove message
                    
                };
            DependencyService.Get<IMessageService>().OnUpdate
                += (message) =>
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        // replace message
                        var toR = Messages.FirstOrDefault(m => m.Key == message.Key);
                        toR.Content = message.Content;
                    });
                    
                };
            //LoadItems();
            //test
        }

        private async void LoadItems()
        {
            var collection = await DependencyService.Get<IMessageService>().GetAllAsync();
            Messages.Clear();
            foreach (var message in collection)
            {
                Messages.Add(message);
            }
        }

        private void Send()
        {
            DependencyService.Get<IMessageService>()
                .AddAsync(new Message {
                    Content = Content,
                    Author = "Khun",
                    Date = DateTime.Now
                });
            Content = null;
        }
    }
}
