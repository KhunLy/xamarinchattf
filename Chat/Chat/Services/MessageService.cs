﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using Chat.Models;
using Firebase.Database;
using Firebase.Database.Streaming;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace Chat.Services
{
    class MessageService : IMessageService
    {
        public event Action<Message> OnAdd;
        public event Action<Message> OnDelete;
        public event Action<Message> OnUpdate;

        private FirebaseClient _Client;

        private List<Message> _Messages
            = new List<Message>();

        public MessageService()
        {
            _Client = DependencyService
                .Get<IFirebaseDbProvider>()
                .GetClient();
            _Client.Child("Messages")
                .AsObservable<Message>()
                .Subscribe((ev) => {
                    if(ev.Object == null)
                    {
                        return;
                    }
                    Message message = _Messages
                            .FirstOrDefault(m => m.Key == ev.Key);
                    if (ev.EventType == FirebaseEventType.Delete)
                    {
                        OnDelete?.Invoke(message);
                    }
                    else if (ev.EventType == FirebaseEventType.InsertOrUpdate)
                    {
                        
                        if(message == null)
                        {
                            Message newM = new Message
                            {
                                Key = ev.Key,
                                Date = ev.Object.Date,
                                Content = ev.Object.Content,
                                Author = ev.Object.Author
                            };
                            _Messages.Add(newM);
                            OnAdd?.Invoke(newM);
                        }
                        else
                        {
                            message.Content = ev.Object.Content;
                            OnUpdate?.Invoke(message);
                        }
                    }
                });
            
        }

        public async Task<string> AddAsync(Message message)
        {
            var result = await _Client.Child("Messages")
                .PostAsync(JsonConvert.SerializeObject(message));

            _Messages.Add(message);
            return result.Key;
        }

        public void Delete(string key)
        {
            _Client.Child("Messages/" + key).DeleteAsync();
        }

        public async Task<List<Message>> GetAllAsync()
        {
            var elems = await _Client.Child("Messages").OnceAsync<Message>();
            foreach (var item in elems)
            {
                _Messages.Add(new Message
                {
                    Key = item.Key,
                    Author = item.Object.Author,
                    Content = item.Object.Content,
                    Date = item.Object.Date
                });
            }
            return _Messages;
        }

        public void Update(Message message)
        {
            _Client.Child("Messages/" + message.Key)
                .PutAsync(JsonConvert.SerializeObject(message));
        }
    }
}
