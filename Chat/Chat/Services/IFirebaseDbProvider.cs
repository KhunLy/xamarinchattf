﻿using Firebase.Database;

namespace Chat.Services
{
    interface IFirebaseDbProvider
    {
        FirebaseClient GetClient();
    }
}