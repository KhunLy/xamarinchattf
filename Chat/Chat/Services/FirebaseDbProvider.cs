﻿using Firebase.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace Chat.Services
{
    class FirebaseDbProvider : IFirebaseDbProvider
    {
        public FirebaseClient GetClient()
        {
            return new FirebaseClient("https://xamarinchat-90bc0.firebaseio.com/");
        }
    }
}
