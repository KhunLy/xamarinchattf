﻿using Chat.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace Chat.Services
{
    interface IMessageService
    {
        Task<List<Message>> GetAllAsync();
        Task<string> AddAsync(Message message);
        void Delete(string key);
        void Update(Message message);

        event Action<Message> OnAdd;

        event Action<Message> OnDelete;

        event Action<Message> OnUpdate;
    }
}
